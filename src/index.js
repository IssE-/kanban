import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';

/* Some thoughts about the project */

// Flux:
// I still don't exactly understand how flux works
// mainly how the stores know what to update
// and how the actions gets triggered.
// this seems to be something flux does by itself.

// Router:
// The book had a older version and as this project has a newer react version,
// the books router was no longer supported.
// Had to use the newer router version and for that reason,
// modality does not work when editing or making a newer card.

// import { Router, Route } from 'react-router';
// 'react-router => 'react-router-dom these days.
import { BrowserRouter as Router} from 'react-router-dom'

//import App from './App';
import KanbanBoardContainer from './components/KanbanBoardContainer';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
    <Router>
            <KanbanBoardContainer/>
    </Router> 
    , document.getElementById('root'));


// The problem: the Route path="/new" and path="/edit/:card_id", are not getting props from KanbanBoardContainer.
// This is because they are not children for KanbanBoardContainer, as this is not supported by Router v4 anymore
// I have a newer version of react, as I used facebooks create-react-app. This version does not support react-router
// but rather react-router-dom. Will focus on next chapter and see if I can fix the problem later.

// Otherwise works now, but, EditCard does not get card props or history from KanbanBoardContainer.
//      -Tommi
/*
ReactDOM.render((
    
    <Router>
        <Switch>
            <Route exact path="/"  render={(props) => ( <KanbanBoardContainer {...props} children={<KanbanBoard/>} /> )}/>
            <Route exact path="/new" component={(props) => ( <KanbanBoardContainer {...props} children={<NewCard/>} /> )} />
            <Route  exact path="/edit/:card_id" 
                    render={(props) => ( <EditCard {...props} cards={<KanbanBoardContainer />} /> )} />
        </Switch>
    </Router>
), document.getElementById('root'));
*/

registerServiceWorker();
