import React,{Component} from 'react';
import CardForm from './CardForm';
import DraftStore from '../stores/DraftStore';
import {Container} from 'flux/utils';
import PropTypes from 'prop-types'
import CardActionCreators from '../actions/CardActionCreators';

class NewCard extends Component {

    handleChange(field, value) {
        CardActionCreators.updateDraft(field, value);
    }
    handleSubmit(e) {
        e.preventDefault();
        CardActionCreators.addCard(this.state.draft);
        this.props.history.push('/')
    }
    handleClose(e) {
        this.props.history.push('/')
    }
    componentDidMount(){
        setTimeout(()=>CardActionCreators.createDraft(), 0)
    }
    
    render(){
        let cardModel=this.props.children && React.cloneElement(this.props.children, {
        })

            return (
                <CardForm   draftCard={this.state.draft}
                            buttonLabel="Create Card"
                            handleChange={this.handleChange.bind(this)}
                            handleSubmit={this.handleSubmit.bind(this)}
                            handleClose={this.handleClose.bind(this)} />
        );
    }
}

NewCard.getStores = () => ([DraftStore]);
NewCard.calculateState = (prevState) => ({
    draft: DraftStore.getState()
});

NewCard.propTypes = {
    cardCallbacks: PropTypes.object,
};

export default Container.create(NewCard);