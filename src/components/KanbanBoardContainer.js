import React, { Component } from 'react';
import update from 'react-addons-update';
import KanbanBoard from './KanbanBoard';
import {throttle} from '../utils';

import EditCard from './EditCard';
import NewCard from './NewCard';
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom'

import {Container} from 'flux/utils';
import CardActionCreators from '../actions/CardActionCreators';
import CardStore from '../stores/CardStore';

// Polyfills
import 'babel-polyfill';
import 'whatwg-fetch';

// removed stringify, add if problems
//import { stringify } from 'querystring';

const API_URL = 'http://kanbanapi.pro-react.com';
const API_HEADERS = {
    'Content-Type': 'application/json',
    Authorization: 'IssE'
};

class KanbanBoardContainer extends Component {


    componentDidMount() {
        CardActionCreators.fetchCards();
    }
    
    render () {
        
        return (
            <Router>
            <Switch>
                <Route exact path="/" render={ (props) => 
                    // to make the modal work prolly have to somehow get the kanbanboard
                    // as a child to NewCard and Editcard
                    // fix if got the time
                    <KanbanBoard    cards= {this.state.cards}
                                    {...props} />}/>

                <Route path="/new" component={(props) => 
                    <NewCard    cards= {this.state.cards}
                                {...props} />}/>

                <Route path="/edit/:card_id" component={(props) => 
                    <EditCard   cards= {this.state.cards}
                                {...props} />}/>
            </Switch>
            </Router>
        )
    }
}
KanbanBoardContainer.getStores = () => ([CardStore]);
KanbanBoardContainer.calculateState = (prevState) => ({
 cards: CardStore.getState()
});
export default Container.create(KanbanBoardContainer);