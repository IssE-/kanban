import React, { Component } from 'react';
import List from './List';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom';

class KanbanBoard extends Component {
    
    render(){

        return (
            <div className="app">
            <Link to='/new' className="float-button">+</Link>
            
            <List   id="todo" title= "To Do" cards={
                    this.props.cards.filter((card) => card.status === "todo")
            }/>

            <List   id="in-progress" title= "In progress" cards = {
                    this.props.cards.filter((card) => card.status === "in-progress")
            } /> 

            <List   id="done" title= "Done" cards={
                    this.props.cards.filter((card) => card.status === "done")
            } />

            {/* There is a problem with the props.chidren
                As the router version is different as in the book,
                the components don't get childrens in the same way as before
                If I got time, I can fixt this. should be fixed by checking at the 
                props sent by router in container. -Tommi */}
            {this.props.children}
            </div>
        )
    }
}

KanbanBoard.propTypes = {
    cards: PropTypes.arrayOf(PropTypes.object)
};

export default DragDropContext(HTML5Backend)(KanbanBoard);