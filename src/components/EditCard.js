import React,{Component} from 'react';
import CardForm from './CardForm'
import CardStore from '../stores/CardStore';
import DraftStore from '../stores/DraftStore';
import {Container} from 'flux/utils';
import CardActionCreators from '../actions/CardActionCreators';
import PropTypes from 'prop-types'

class EditCard extends Component {

    handleChange(field,value) {
        CardActionCreators.updateDraft(field, value);
    }
    
    handleSubmit(e) {
        e.preventDefault();
        CardActionCreators.updateCard(
            CardStore.getCard(this.props.match.params.card_id),this.state.draft
        );
        this.props.history.push('/')
    }

    handleClose(e) {
        this.props.history.push('/')
    }
    componentDidMount(){
        setTimeout(()=>{
            CardActionCreators.createDraft(CardStore.getCard(this.props.match.params.card_id))
        }, 0);
     }

    render() {
        return (
            <CardForm   draftCard={this.state.draft}
                        buttonLabel="Edit Card"
                        handleChange={this.handleChange.bind(this)}
                        handleSubmit={this.handleSubmit.bind(this)}
                        handleClose={this.handleClose.bind(this)} />
        )
    }
}

EditCard.getStores = () => ([DraftStore]);
EditCard.calculateState = (prevState) => ({
    draft: DraftStore.getState()
});

EditCard.propTypes = {
    cardCallbacks: PropTypes.object,
}

export default Container.create(EditCard);
