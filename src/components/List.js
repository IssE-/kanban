import React, { Component } from 'react';
import Card from './Card';
import PropTypes from 'prop-types'
import { DropTarget } from 'react-dnd';
import constants from '../Constants';
import CardActionCreators from '../actions/CardActionCreators';

const listTargetSpec = {
    hover(props, monitor) {
        const dragged = monitor.getItem();
        CardActionCreators.updateCardStatus(dragged.id, props.id);
    }
};

function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget()
    };
}

/**
 * List that renders all the cards
 */

class List extends Component {
    render() {
        const { connectDropTarget } = this.props;

        let cards = this.props.cards.map((card) => {
            return <Card key={card.id}{...card} />
        });

        return connectDropTarget(
            <div className ="list">
            <h1> {this.props.title} </h1>
                {cards}
            </div>
        );
    }
}

List.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    cards: PropTypes.arrayOf(PropTypes.object),
    connectDropTarget: PropTypes.func.isRequired
};

export default DropTarget(constants.CARD, listTargetSpec, collect)(List);